const passport = require('passport');
const User = require('../models/user');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});


passport.use('local.signup', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function (req, email, password, done) {
  User.findOne({ 'email': email }, (err, user) => {

    if (err) {
      return done(err);
    }

    if (user) {
      return done(null, false, { message: 'Account already exists' });
    }

    let newUser = new User();
    newUser._id = new mongoose.Types.ObjectId();
    newUser.email = email;
    newUser.passport = newUser.encryptPassword(password);
    newUser.name = req.param('name');

    newUser.save((err, result) => {
      if (err) {
        console.log(err);
        return done(err);
      }

      req.session.username = email;
      return done(null, newUser);
    });

  });
}));




passport.use('local.signin', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function (req, email, password, done) {
  User.findOne({ 'email': email }, (err, user) => {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false, { message: 'no user found' });
    }
    if (!user.validPassword(password)) {
      return done(null, false, { message: 'Wrong password' });
    }
    req.session.username = email;
    return done(null, newUser);
  })
}));
