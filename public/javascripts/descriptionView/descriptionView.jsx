'use strict'

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Col, Row, Badge } from 'reactstrap'
import { search_by_id } from '../actions/searchAction.js'
import Ratings from './ratings.jsx';
import _ from 'lodash' 

class DescriptionView extends React.Component{
    constructor(props){
        super(props);

        this.getSelected = this.getSelected.bind(this);
    }

    getSelected(id){
        this.props.search_by_id(id);
    }

    render(){
        let itemShow;
        if(!_.isEmpty(this.props.item)){
            return(
                <div className="description-view-item container">
                    {/* Row 1 for poster and title */}
                    <div className="description-head">
                        <Row>
                            <Col md={4} sm="12">
                                <img src={this.props.item.Poster} alt="" />
                            </Col>

                            {/* <div className="tags-title"> */}
                                <Col md={8} sm="12">
                                    <div className="movie-title">
                                        <h3>{this.props.item.Title}</h3>
                                    </div>
                                    <div className="movie-tags">
                                        <ol id="tags-list">
                                            <li><Badge color="secondary">{this.props.item.Rated}</Badge></li>
                                            <li>{this.props.item.Year}</li>
                                            <li>{this.props.item.Genre}</li>
                                            <li>{this.props.item.Runtime}</li>
                                        </ol>
                                    </div>
                                </Col>
                            {/* </div> */}

                        </Row>
                    </div>


                    {/* Row 2 for description */}
                    <Row>
                        <Col md={12} xs={12}>
                            <div className="movie-plot">
                                <p>
                                    {this.props.item.Plot}
                                </p>
                            </div>
                        </Col>
                    </Row>

                    {/* Row 3 for ratings */}
                    <Row>
                        <Ratings data={this.props.item.Ratings} />
                    </Row>
                </div>
            )
        }
        else{
            return(
                <>
                </>
            )
        }
        return(
            <>
                {itemShow}
            </>
        );
    };
}

DescriptionView.prototypes = {
    //functions
    search_by_id: PropTypes.func.isRequired,

    //states
    item: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    item: state.searchAction.selected_item
});

export default  connect(mapStateToProps, {search_by_id} ) (DescriptionView);