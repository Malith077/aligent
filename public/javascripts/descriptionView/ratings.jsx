'use strict'
import React from 'react';
import { Col, Row } from 'reactstrap';

class Ratings extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let data = this.props.data;
        let ratingView;
        if(data.length > 0) {
            ratingView = data.map(rate => {
                return(
                    <>
                        <div key={rate.id}>
                            <Col sm={4} xs={12}>
                                <p>{rate.Value}</p>
                                <small>{rate.Source}</small>
                            </Col>
                        </div>
                        
                    </>
                )
            })
        }
        return(
            <>
                {ratingView}
            </>
        )
    }
}

export default Ratings;

