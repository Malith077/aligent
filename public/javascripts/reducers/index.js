/**
 * index.js - contains references to all the reducers
 */

import {
    combineReducers
} from 'redux';

import searchReducer from './searchReducer.js';

export default combineReducers({
    searchAction : searchReducer
});