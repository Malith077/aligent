import { SEARCH, RESULT, RESULT_BY_ID} from '../actions/types';

const initialState = {
    results: [],
    selected_item:{}
};

export default function (state = initialState, action) {
    switch(action.type){
        case RESULT:
            return{
                ...state,
                results: action.payload
            };
        case RESULT_BY_ID:
            return{
                ...state,
                selected_item: action.payload
            };
        default:
            return state;
    }
}

