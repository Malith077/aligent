"use strict"

require("babel-polyfill");

import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux';
import store from './store.js';

import 'bootstrap'

//React Componenets
import NavigationBar from './nav/nav.jsx';
import SearchResult from './searchResults/searchResults.jsx'
import DescriptionView from './descriptionView/descriptionView.jsx'

//Mounting target
const navLocation = document.getElementById('navbar-location');
const searchResult = document.getElementById('search-results');
const searchPreview = document.getElementById('search-preview');


if (navLocation) {
    ReactDOM.render(
        <Provider store={store}>
            <NavigationBar />
        </Provider>,
        navLocation
    )
}

if(searchResult){
    ReactDOM.render(
        <Provider store={store}>
            <SearchResult />
        </Provider>,
        searchResult
    )
}

if(searchPreview){
    ReactDOM.render(
        <Provider store={store}>
            <DescriptionView />
        </Provider>,
        searchPreview
        )
}
