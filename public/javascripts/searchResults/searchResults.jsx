'use strict'

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ResutlCard from '../resultCard/resultCard.jsx'
 
class SearchResults extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let totalResutls;
        if(this.props.results.Response == "True")
        {
            if(this.props.results.Search.length > 0){
                totalResutls = this.props.results.Search.map(searched =>{
                    return(
                        <div key={searched.id}>
                            <ResutlCard data={searched} />
                        </div>
                    )
                })
            }

        }
        else{
            return(
                <>
                    <div className="empty-string">
                        <p>Start typing to search!</p>
                    </div>
                </>
            )
        }
        return(
            <div>
                <div className="total-results">
                    <p>{this.props.results.totalResults} RESULTS</p>
                </div>
                <div className="results-body">
                    {totalResutls}
                </div>
            </div>
        )

    };
}

SearchResults.prototypes={
    //functions

    //states
    results: PropTypes.array.isRequired
}

const mapStateToProps = (state) => ({
    results: state.searchAction.results
});

export default connect(mapStateToProps,{})(SearchResults);
