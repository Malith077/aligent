'use strict'

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Col, Row } from 'reactstrap'
import { search_by_id } from '../actions/searchAction.js'
 
class ResutlCard extends React.Component{
    constructor(props){
        super(props);

        this.getSelected = this.getSelected.bind(this);
    }

    getSelected(id){
        this.props.search_by_id(id);
    }

    render(){
        const {data} = this.props;
        return(
            <>
                <Row className="result-card" onClick={() => {this.getSelected(data.imdbID)}}>
                    <Col md={3} xs={3}>
                        <img className="card-image" src={data.Poster} alt="poster" height='100' width='100' />
                    </Col>
                    <Col md={9} xs={9}>
                        <div className="card-body">
                            <p className="movie-title">{data.Title}</p>
                            <p className="movie-year">{data.Year}</p>
                        </div>
                    </Col>
                </Row>
            </>
        );
    };
}

ResutlCard.prototypes = {
    //functions
    search_by_id: PropTypes.func.isRequired,

    //states
    item: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    item: state.searchAction.selected_item
});

export default  connect(mapStateToProps, {search_by_id} ) (ResutlCard);
