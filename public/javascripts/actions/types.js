export const SEARCH = 'SEARCH';
export const SEARCH_BY_ID = 'SEARCH_BY_ID';
export const RESULT = 'RESULT';
export const RESULT_BY_ID = 'RESULT_BY_ID';