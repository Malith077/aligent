import { SEARCH, SEARCH_BY_ID, RESULT, RESULT_BY_ID } from './types.js';

//Searching with movie name
export const search = (query) => dispatch => {
    fetch(`http://www.omdbapi.com/?apikey=42d82d47&s=${query}`,{
        method:'POST'
    })
    .then(res => res.json())
    .then(object => dispatch({
        type: RESULT,
        payload: object
    }));

}

//Searching with movie ID
export const search_by_id = (id) => dispatch => {
    fetch(`http://www.omdbapi.com/?apikey=42d82d47&i=${id}&plot=full`,{
        method: 'POST'
    })
    .then(res => res.json())
    .then(object => dispatch({
        type: RESULT_BY_ID,
        payload: object
    }));
}