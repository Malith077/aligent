'use strict'

import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  Form,
  FormGroup,
  Input,
  Label
} from 'reactstrap';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { search } from '../actions/searchAction.js';

class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.handleChange = this.handleChange.bind(this);


    this.state = {
      isOpen: false,
      searchQuery: ""
    };
  }

  handleChange(event) {
    this.props.search(event.target.value);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <>
        <Navbar color="light" light expand="md">
            <Form inline>
              <FormGroup>
              <Label for="search" className="mr-sm-2"><i class="material-icons">search</i></Label>
                <Input type="text" name="text" id="seach" placeholder="Type to search" onChange={event => this.handleChange(event)}/>
              </FormGroup>
            </Form>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>

              </NavItem>
              
            </Nav>
          </Collapse>
        </Navbar>
      </>
    )
  }
}

NavigationBar.prototypes={
  //functions
  search: PropTypes.func.isRequired,

  //states
  results: PropTypes.array.isRequired
}

const mapStateToProps = (state) => ({
  searchResults: state.searchAction.results
});

export default connect(mapStateToProps, {search}) (NavigationBar);