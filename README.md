# Aligent Code interview

First you need to `clone` the project to your local device
```bash
git clone git@gitlab.com:Malith077/aligent.git
```

Then in the aligent directory
```bash
npm install
```

Afterwards, you need to build the project
```bash
npm run build
```

Finally, in order to run the application on port 3000 run the following command
```bash
npm run start
```
