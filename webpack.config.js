// const path = require('path');
// const webpack = require('webpack');

// module.exports = {
//   module: {
//     rules: [
//       {
//         test: /\.(js|jsx)$/,
//         exclude: /node_modules/,
//         use: [
//           {
//             loader: 'babel-loader',
//             options: {
//               presets: [
//                 '@babel/preset-env',
//                 {
//                   plugins: [
//                     '@babel/plugin-proposal-class-properties'
//                   ]
//                 }
//               ]
//             },
//           }]
//       },
//       {
//         test: /\.css$/i,
//         use: ['style-loader', 'css-loader']
//       },
//       {
//         test: /\.sass$/,
//         exclude: /node_modules/,
//         use: ['style-loader',
//           {
//             loader: 'css-loader',
//             options: {
//               modules: true,
//               sourceMap: true,
//               localIdentName: '[name]__[local]__[hash:base64:5]'
//             }

//           },
//           {
//             loader: 'sass-loader',
//             options: {
//               indentedSyntax: true
//             }
//           }
//         ]
//       }
//     ]

//   },
//   resolve: {
//     alias: {
//       styles: path.join(__dirname, 'public/css')
//     }
//   }
//   ,
//   node: {
//     __dirname: false,
//     __filename:
//       true,
//     fs:
//       'empty'
//   },
//   entry: path.resolve(__dirname) + '/public/javascripts/index.js',
//   output:
//     {
//       filename: '[name].bundle.js',
//       chunkFilename:
//         '[name].bundle.js',
//       path:
//         path.resolve(__dirname, 'public/dist'),
//       sourceMapFilename:
//         'main.map',
//       publicPath:
//         '/dist/'
//     },
//   plugins: [
//     new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-gb/),

//     new webpack.ProvidePlugin({
//       $: 'jquery',
//       jQuery: 'jquery',
//       'window.jQuery': 'jquery',
//       Popper: ['popper.js', 'default'],
//       'window.Quill': 'quill',
//     })
//   ]
// };



const path = require('path');
const webpack = require('webpack');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.sass$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: true,
              localIdentName: '[name]__[local]__[hash:base64:5]'
            }
          },
          {
            loader: 'sass-loader',
            options: {
              indentedSyntax: true
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      styles: path.join(__dirname, 'public/css')
    }
  },
  node: {
    __dirname: false,
    __filename: true
  },
  mode: "production",
  entry: path.resolve(__dirname) + '/public/javascripts/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'public/dist'),
    sourceMapFilename: 'main.map'
  },
  devtool: '#source-map',
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default'],
      'window.Quill': 'quill',
    })
  ]
};
